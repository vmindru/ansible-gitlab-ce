# Install gitlab-ce with ansible.

# Setup hosts

Create a hosts file.

    [gitlab-ce]
    gitlab-ce.example.com

Or do some customization.

    [gitlab-ce]
    gitlab-ce ansible_host=X.X.X.X gitlab_repo=unstable gitlab_version=9.5.1-ce.0

# Deploy

    ansible-playbook site.yml
